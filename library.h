typedef struct player {
	char playerName[20];
	char discType;
	int discNumber;
} player;

typedef struct disc {
	int discColour;
	int xPos;
	int yPos;
} disc;