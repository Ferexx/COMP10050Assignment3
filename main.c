/* A program that implements the Reversi game in C,
    written by Jack Price and Monika Pociute. */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "library.h"

#define notplayerTurn(x) ((x)==('W')?('B'):('W'))       //Macro which returns the colour of the player whose turn it isn't

int x_1, y_1;
char xMoves[63];
int yMoves[63];
char theBoard[7][7];
int k;

void printBoard(void);
void printPossibleMoves(void);                                   //See documentation for description of what each function does
void clearArray(void);
void possibleMoves(char playerTurn);
void updateBoard(int x1, int y1, char turn);
void endGame(player first, player second);
bool checkMove(void);
int convertToCoords(char input);
int nearby(int x, int y, char turn);


int main(void) {
    int ymove, turns=0; char xmove;
    theBoard[3][3]='W'; theBoard[4][4]='W';         //Initial position of discs on board
    theBoard[3][4]='B'; theBoard[4][3]='B';
    player player1, player2;
    printf("Player 1, please enter your name: ");   //Getting player names and assignment of disc colors.
    scanf("%[^\n]s", player1.playerName);
    fseek(stdin, 0, SEEK_END);
    printf("Player 2, please enter your name: ");
    scanf("%[^\n]s", player2.playerName);
    player1.discType='B';
    player2.discType='W';
    printBoard();
    while(1) {                                      //Game main loop
        possibleMoves('B');                         //Compute possible moves, print them, then ask the player which move they'd like to make
        printPossibleMoves();
        if(k!=0) {                                  //This if statement is meant to check for the end of the game, but k is never equal to 0 for some reason
            printf("Where would you like to move, %s?\n", player1.playerName);
            fseek(stdin, 0, SEEK_END);
            scanf("%c", &x_1);
            xmove = convertToCoords(x_1);           //Convert from A,B,C,D etc to a number between 1 and 8 inclusive
            scanf("%d", &ymove);
            y_1=ymove;
            while(checkMove()==false) {             //Loop while the player inputs an invalid move
                printf("Invalid move!\n");
                printf("Where would you like to move, %s?\n", player1.playerName);
                fseek(stdin, 0, SEEK_END);
                scanf("%c", &x_1);
                xmove = convertToCoords(x_1);
                scanf("%d", &ymove);
                y_1=ymove;
            }
            updateBoard(ymove-1, xmove-1, 'B');
        }
        else
            break;
        printBoard();
        clearArray();
        turns++;

        possibleMoves('W');                         //Now white's turn
        printPossibleMoves();
        if (k!=0) {
            printf("Where would you like to move, %s?\n", player2.playerName);
            fseek(stdin, 0, SEEK_END);
            scanf("%c", &x_1);
            xmove = convertToCoords(x_1);
            scanf("%d", &ymove);
            y_1=ymove;
            while(checkMove()==false) {
                printf("Invalid move!\n");
                printf("Where would you like to move, %s?\n", player2.playerName);
                fseek(stdin, 0, SEEK_END);
                scanf("%c", &x_1);
                xmove = convertToCoords(x_1);
                scanf("%d", &ymove);
                y_1 = ymove;
                }
            updateBoard(ymove-1, xmove-1, 'W');
        }
        else
            break;
        printBoard();
        clearArray();

        turns++;
        if(turns>63)
            break;
    }
    endGame(player1, player2);                      //Once main game loop exits, execute endgame actions
}

void printBoard(void) {
    printf("\n0 A B C D E F G H\n");                //Displaying board in console.
    for(int i=0;i<8;i++) {
        printf("%d", i+1);
        for(int j=0;j<8;j++) {
            if(j==7&&(theBoard[i][j]=='B'||theBoard[i][j]=='W')) {      //Special cases if printing has reached end of line.
                printf("|%c|\n", theBoard[i][j]);
                break;
            }
            else if(j==7)
                printf("|x|\n");
            else if(theBoard[i][j]=='B'||theBoard[i][j]=='W'){          //Standard printing board.
                printf("|%c", theBoard[i][j]);
            }
            else {
                printf("|x");
            }
        }
    }
    printf("\n");
}

void possibleMoves(char playerTurn) {
    k=0;
    int potential, xcheck, ycheck;
    char xconvert;
    printf("Your possible moves are as follows: ");
    for(int i=0;i<8;i++) {
        for(int j=0;j<8;j++) {
            if(theBoard[j][i]==playerTurn) {                            //Search through board until we find a disk of the turn player's colour
                potential=1;
                while(potential!=0) {
                    potential = nearby(j, i, notplayerTurn(playerTurn));    //Find a disk of the opposite colour within one square of the disk we found earlier
                    xcheck=i; ycheck=j;
                    if(potential!=0) {
                        switch (potential) {                               //Based on what direction we find a disk of the opposite colour, keep iterating in that direction until we find something that isn't the opposite players disk
                        case 1:
                            while(theBoard[--ycheck][--xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}     //Iterate in direction
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {                                              //Once we find something that isn't opposite player disk, check if it's a blank space
                                yMoves[k] = (ycheck+1);                                                                                     //Log that position
                                xconvert = xcheck+65;                                                                                       //Conversion back to character
                                xMoves[k] = xconvert;
                                k++;                                                                                                        //Increase k for use in endgame conditions
                            }
                            break;
                        case 2:
                            while(theBoard[--ycheck][xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 3:
                            while(theBoard[--ycheck][++xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 4:
                            while(theBoard[ycheck][++xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 5:
                            while(theBoard[++ycheck][++xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 6:
                            while(theBoard[++ycheck][xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 7:
                            while(theBoard[++ycheck][--xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        case 8:
                            while(theBoard[ycheck][--xcheck]==notplayerTurn(playerTurn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                            if(theBoard[ycheck][xcheck]!='B'&&theBoard[ycheck][xcheck]!='W') {
                                yMoves[k] = (ycheck+1);
                                xconvert = xcheck+65;
                                xMoves[k] = xconvert;
                                k++;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    if(k==0) {                                                                                           //Check for end of game
        printf("You have no possible moves.\n");
    }
}

void printPossibleMoves(void) {
    for(int i=0;i<=k;i++) {
        for(int j=0;j<=k;j++) {
            if(xMoves[j]==xMoves[i]&&yMoves[j]==yMoves[i]&&i!=j) {                                       //Iterate through two storage arrays and delete any duplicate moves
                xMoves[j]=0;
                yMoves[j]=0;
            }
        }
    }
    for(int i=0;i<=k;i++) {                                                                              //Iterate through two arrays, printing as we go
        if(xMoves[i]<73&&xMoves[i]>64&&yMoves[i]<9&&yMoves[i]>0)
            printf("%c%d ", xMoves[i], yMoves[i]);
    }
    printf("\n");
}

void updateBoard(int y1, int x1, char turn) {
    int potential=1, xcheck, ycheck, xupdate, yupdate;
    while(potential!=0) {
            potential = nearby(y1, x1, notplayerTurn(turn));                                            //Find disk within one square of opposite players color
            xcheck = x1; ycheck = y1;
            if(potential!=0) {
                switch (potential) {
                case 1:
                    while(theBoard[--ycheck][--xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}       //Iterate until we find something that isn't opposite players disc
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck&&xupdate!=xcheck) {                                       //Provided what we've found is a turn player's disc, flip discs, starting from position player indicated
                            theBoard[yupdate][xupdate]=turn;
                            --yupdate; --xupdate;
                        }
                    }
                    break;
                case 2:
                    while(theBoard[--ycheck][xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck) {
                            theBoard[yupdate][xupdate]=turn;
                            --yupdate;
                        }
                    }
                    break;
                case 3:
                    while(theBoard[--ycheck][++xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck&&xupdate!=xcheck) {
                            theBoard[yupdate][xupdate]=turn;
                            --yupdate; ++xupdate;
                        }
                    }
                    break;
                case 4:
                    while(theBoard[ycheck][++xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(xupdate!=xcheck) {
                            theBoard[yupdate][xupdate]=turn;
                            ++xupdate;
                        }
                    }
                    break;
                case 5:
                    while(theBoard[++ycheck][++xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck&&xupdate!=xcheck) {
                            theBoard[yupdate][xupdate]=turn;
                            ++yupdate; ++xupdate;
                        }
                    }
                    break;
                case 6:
                    while(theBoard[++ycheck][xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck) {
                            theBoard[yupdate][xupdate]=turn;
                            ++yupdate;
                        }
                    }
                    break;
                case 7:
                    while(theBoard[++ycheck][--xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(yupdate!=ycheck&&xupdate!=xcheck) {
                            theBoard[yupdate][xupdate]=turn;
                            ++yupdate; --xupdate;
                        }
                    }
                    break;
                case 8:
                    while(theBoard[ycheck][--xcheck]==notplayerTurn(turn)&&xcheck>0&&xcheck<=8&&ycheck>0&&ycheck<=7) {}
                    if(theBoard[ycheck][xcheck]==turn) {
                        xupdate=x1; yupdate=y1;
                        while(xupdate!=xcheck) {
                            theBoard[yupdate][xupdate]=turn;
                            --xupdate;
                        }
                    }
                    break;
                }
            }
    }
}

int nearby(int y, int x, char turn) {
    static int direction=0;                                     //Static so that program doesn't same direction twice
    while(1) {
        if(theBoard[y-1][x-1]==turn&&direction<1) {             //Ignore if we already checked it on a previous function call
            direction = 1; break; }
        if(theBoard[y-1][x]==turn&&direction<2) {
            direction = 2; break; }
        if(theBoard[y-1][x+1]==turn&&direction<3) {
            direction = 3; break; }
        if(theBoard[y][x+1]==turn&&direction<4) {
            direction = 4; break; }
        if(theBoard[y+1][x+1]==turn&&direction<5) {
            direction = 5; break; }
        if(theBoard[y+1][x]==turn&&direction<6) {
            direction  = 6; break; }
        if(theBoard[y+1][x-1]==turn&&direction<7) {
            direction = 7; break; }
        if(theBoard[y][x-1]==turn&&direction<8) {
            direction = 8; break; }
        direction = 0;                                      //Once all directions have been checked, reset direction
        break;
    }
    return direction;
}

int convertToCoords(char input) {
    if(input>=65&&input<=72)                                //Lowercase
        return input-64;
    if(input>=97&&input<=104)                               //Uppercase
        return input-96;
    else {
        return 0;
    }
}

bool checkMove(void){
    int i;
    for(i=0;i<63;i++){
        if((x_1 == xMoves[i]) && (y_1 == yMoves[i])){       //Check all positions in storage array
            printf("Valid move!\n");
            return true;
        }
    }
    return false;
}

void clearArray(void) {
    for(int i=0;i<64;i++) {                                 //Delete all values in array so we don't get runover moves on next iteration of main game loop
        xMoves[i] = 'X';
        yMoves[i] = -1;
    }
}

void endGame(player first, player second) {
    FILE *fp;
    fp=fopen("result.txt", "w+");
    int bCount, wCount;
    for(int i=0;i<8;i++) {                                 //Count up discs
        for(int j=0;j<8;j++) {
            if(theBoard[j][i]=='B')
                bCount++;
            if(theBoard[j][i]=='W')
                wCount++;
        }
    }
    if(wCount<bCount) {                                                     //If player 1 wins
        printf("%s has %d discs.\n", first.playerName, bCount);
        fprintf(fp, "%s has %d discs.\n", first.playerName, bCount);
        printf("%s has %d discs.\n", second.playerName, wCount);
        fprintf(fp, "%s has %d discs.\n", second.playerName, wCount);
        printf("%s wins!", first.playerName);
        fprintf(fp, "%s wins!", first.playerName);
    }
    if(bCount<wCount) {                                                     //If player 2 wins
        printf("%s has %d discs.\n", first.playerName, bCount);
        fprintf(fp, "%s has %d discs.\n", first.playerName, bCount);
        printf("%s has %d discs.\n", second.playerName, wCount);
        fprintf(fp, "%s has %d discs.\n", second.playerName, wCount);
        printf("%s wins!", second.playerName);
        fprintf(fp, "%s wins!", second.playerName);
    }
    if(bCount==wCount) {                                                    //If it's a draw
        printf("%s has %d discs.\n", first.playerName, bCount);
        fprintf(fp, "%s has %d discs.\n", first.playerName, bCount);
        printf("%s has %d discs.\n", second.playerName, wCount);
        fprintf(fp, "%s has %d discs.\n", second.playerName, wCount);
        printf("It's a draw!");
        fprintf(fp, "It's a draw!");
    }
    fclose(fp);
}
